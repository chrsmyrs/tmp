# -*- coding: utf-8 -*-
import config
import myloans
from util import myprint
from PlatformTMP import PlatformTMP
import secureRequest
from summary import loadSummary,saveSummary,addToSummary,outputSummaryToCsv

##################################### ENTRYPOINT #####################################################################


# initialise
if config.USE_PROXY:
    secureRequest.init(id, proxies={"http": "http://127.0.0.1:8888", "https": "http:127.0.0.1:8888"})
else:
    secureRequest.init(id, proxies=None)

PlatformMT = PlatformTMP()

myprint("TMP Reset")

if config.LOANS_FROM_WEBSITE:
    # log in to the money platform
    balance = PlatformMT.doLogin()

    # retrieve all the loans data
    loans = PlatformMT.getLoans()

    # save loans to a local file "loans.json"
    myloans.saveLoans(loans)
else:
    # use loan data from the local file
    loans = myloans.loadLoans()
    balance = -1

# output loans data to a csv file
myloans.outputCsv(loans)

# print some basic stats
print("Balance is %d" % balance)
totals = (goodTotal,badTotal,otherTotal) = myloans.calculateTotals(loans)

print("Total Loans: " + str(len(loans)))
print("GOOD   BAD    OTHER")
print(goodTotal,badTotal,otherTotal)

print("\nGood + balance = %f" % (goodTotal+balance))


# Record ongoing summary
summary = loadSummary()
addToSummary(summary, goodTotal+balance)
saveSummary(summary)

outputSummaryToCsv(summary)





