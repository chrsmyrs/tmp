import config
from util import myprint
import json
from time import localtime, strftime

def loadSummary():
    try:
        with open(config.LOCAL_SUMMARY_FILE, 'r') as f:
            summary = json.load(f)
    except IOError:
        myprint("Summary file doesn't exist. Will create a new one")
        summary = {}

    return summary

def saveSummary(summary):
    with open(config.LOCAL_SUMMARY_FILE, 'w') as f:
           json.dump(summary, f, indent=3)


def addToSummary(summary, goodTotal):

    today = strftime("%Y-%m-%d", localtime())

    summary[today] = goodTotal


def outputSummaryToCsv(summary):
        with open(config.CSV_SUMMARY_FILENAME, 'w') as f:
            for day in sorted(summary.keys()):
                f.write("%s, " % day)
                f.write("%f,\n " % summary[day])

