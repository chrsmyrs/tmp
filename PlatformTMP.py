import re



import util
from util import myprint, myerror, mywarn, getLoginFormIds, myinfo
import secureRequest

import config
import time
import myloans

#Login is a 4 request proces. Get/Post to /login and then Get/Post to /security-questions
#Here they are labelled r1,r2,r3 and r4


# MUST DISABLE THIS FOR ANY KIND OF PRODUCTION
# It is supposed to be doing the same checks that chrome does to get the 'green padlock'
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


class PlatformTMP():

    def __init__(self):
        self.sRequest = secureRequest



    def doLogin(self):
        # do we need all these?
        postData = {'_username': config.credentials.USERNAME,
                    '_password': config.credentials.PASSWORD}

        while True:
            ##############Phase 1####################
            r1 = self.sRequest.get(url=config.URL_LOG_IN)


            loginFormIds = getLoginFormIds(r1.text)
            postData = {**postData, **loginFormIds}
            r2 = self.sRequest.post(config.URL_BALANCE, postData)

            balance = self.getBalance(r2.text)

            if balance is not None:
                myprint ("Logged in. Your balance is ", balance)
                break
            else:
                myprint("Couldn't log in. Trying again shortly....")
                time.sleep(10)

        return balance

    # Assumes we're already logged in
    # Then loops through all the loan pages storing the information in a dict called "loans"
    # which is what is returned
    def getLoans(self ):
        loans = []
        pageCount = -1
        pageIndex = 1
        while True:
            myprint("---------------------PAGE: " + str(pageIndex))
            url = config.URL_LOAN_PAGE_BASE + str(pageIndex)

            r = self.sRequest.get(url)
            if r == None:
                return None


            #get pageCount
            if pageCount == -1:
                search = re.search('.*Page\s\d+\s?of (\d+)', r.text)
                if search != None:
                    pageCount = int(search.group(1))

                if pageCount == -1:
                    myprint("Couldn't get page count")
                    exit(1)

            #print(r.text)
            loans += myloans.parseLoans(r.text)

            pageIndex += 1
            if pageIndex > pageCount:
                myprint("All Done!")
                break;


        return loans


    def getBalance(self, text):
            mysearch = re.search('£(\d*,?\d+\.\d\d)', text)

            if (mysearch == None):
                return None
            else:
                balance = float(mysearch.group(1).replace(',', ''))

            return balance

