from bs4 import BeautifulSoup
import config
import json

loansFilename = config.LOCAL_LOANS_FILE

def calculateTotals(loans):

    goodTotal = badTotal = otherTotal = 0

    for loan in loans:
        if 'started' in loan['State']:
            goodTotal += loan['Value']
        elif ('overdue' in loan['State'] or 'defaulted' in loan['State']):
            badTotal += loan['Value']
        else:
            otherTotal += loan['Value']

    return (goodTotal,badTotal,otherTotal)

def outputCsv(loans):
    with open(config.CSV_FILENAME, 'w') as f:
        for loan in loans:
            f.write("%s, " % loan['Id'])
            f.write("%f, " % loan['Value'])
            f.write("%s, " % loan['Rate'])
            f.write("%s, " % loan['Term'])
            f.write("%s, " % loan['Profit'])
            f.write("%s, " % loan['ProfitPercent'])
            f.write("%s, " % loan['State'])
            f.write("%s, \n" % loan['RepaymentDate'])


def saveLoans(loans):
    with open(loansFilename, 'w') as f:
        json.dump(loans, f, indent=3)

def loadLoans():
    try:
        with open(loansFilename, 'r') as f:
            return json.load(f)
    except IOError:
        file = open(loansFilename, 'w')
        loans = []
        json.dump(loans, file, indent=3)
        return []




def parseLoans(text):
    soup = BeautifulSoup(text, 'html.parser')

    # myprint(soup.prettify())

    tableBody = soup.find('tbody')
    rows = tableBody.find_all('tr')
    loans = []
    for row in rows:
        loan = {}
        cols = row.find_all('td')
        loan['Id'] = cols[0].text.strip()
        loan['Value'] = float(cols[1].text.strip().strip('£'))
        loan['Term'] = cols[2].text.strip()
        loan['Rate'] = cols[3].text.strip()
        loan['Profit'] = cols[4].text.strip()
        loan['ProfitPercent'] = cols[5].text.strip()
        loan['RepaymentDate'] = cols[6].text.strip()
        loan['State'] = cols[7].text.strip()

        loans.append(loan)    # Get rid of empty values


    return loans