import pytz
import credentials

#If True will connect to TMP website and download balance and loan data
#Otherwise it uses a local cache "loans.json"
LOANS_FROM_WEBSITE = True

# csv file created by this application
CSV_FILENAME = "loans.csv"
CSV_SUMMARY_FILENAME = "summary.csv"

# local copy of loans data in json format
LOCAL_LOANS_FILE = "loans.json"
LOCAL_SUMMARY_FILE = "summary.json"


#set true if going through fiddler
USE_PROXY = False

MY_TIMEZONE=pytz.timezone('Europe/London')

#list or urls we use
URL_LOG_IN = 'https://themoneyplatform.com/login'
URL_BALANCE = 'https://themoneyplatform.com/login_check'
URL_LOAN_PAGE_BASE = 'https://themoneyplatform.com/private-area/lender/loans/summary/'

#set browser headers to make us look a bit more like a regular browser
BROWSER_HEADERS = {'Host': 'themoneyplatform.com',
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:76.0) Gecko/20100101 Firefox/76.0',
                'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
                'Accept-Language':  'en-GB,en;q=0.5',
                'Accept-Encoding': 'gzip, deflate, br',
                'Content-Type': 'application/x-www-form-urlencoded',
                'Upgrade-Insecure-Requests' : '1',
                'DNT': '1',
                }


